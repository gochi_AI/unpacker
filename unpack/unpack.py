import os
import json
import shutil
import logging
import collections

import etcpack
import unitypack
from PIL import Image, ImageOps

from .utils import gen_thumbnail, combine_rgba
from .json_utils import KiraraJSONEncoder


undefined_types = {'Font', 'GameObject', 'Material', 'Shader'}


class Unpacker:

    def __del__(self):
        self.f.close()

    def __init__(self, muast, types=['png'], verbose=False):
        self.muast = muast
        self.types = types
        self.verbose = verbose

        self.textures = {}
        self.sprites = {}
        self.map_texture_sprite = {}

        self.f = open(muast, 'rb')
        self.objects = unitypack.load(self.f).assets[0].objects

        self.image_alpha_dict = {}
        self.image_rgb_dict = {}

    def _Texture2D(self, texture):
        if texture.name in self.textures:
            return self.textures[texture.name]
        img = etcpack.etcpack(texture)

        self.textures[texture.name] = img
        return img

    def _Sprite(self, sprite, name):
        texture = self.objects[sprite.rd['texture'].path_id].read()
        texture = self._Texture2D(texture)
        box = (
            sprite.rect['x'],
            sprite.rect['y'],
            sprite.rect['x'] + sprite.rect['width'],
            sprite.rect['y'] + sprite.rect['height'],
        )
        img = texture.crop(box)

        path = os.path.splitext(name)[0] + '/' + sprite.name
        if name not in self.map_texture_sprite:
            self.map_texture_sprite[name] = [path]
        else:
            self.map_texture_sprite[name].append(path)
        self.sprites[path] = img

        return img

    def __call__(self):
        flag = True

        for path_id in (1, 2):
            if self.objects[path_id].type == 'AssetBundle':
                asset_bundle = self.objects[path_id].read()
                container = asset_bundle['m_Container']
                break
        else:
            flag = False
            logging.error('Cannot find Asset Bundle. Found in %s' % self.muast)

        for name, item in container:
            asset = item['asset']
            if asset is None:
                continue
            obj = self.objects[asset.path_id]
            data = obj.read()

            if self.verbose:
                logging.info("%s: Unpacking %s %s" % (
                    self.muast, obj.type, name))

            if obj.type in undefined_types:
                flag = False
                continue

            elif obj.type == 'Texture2D':
                self._Texture2D(data)

            elif obj.type == 'Sprite':
                self._Sprite(data, name)

            elif type(data) is collections.OrderedDict and 'json' in self.types:
                path = '%s/%s.json' % (os.path.dirname(name), data['m_Name'])
                if 'm_Params' in data:
                    self.save_json(path, data['m_Params'])
                else:
                    self.save_json(path, data)

            else:
                flag = False
                undefined_types.add(obj.type)
                logging.warning('Type %s is not defined. Found in %s.%s' % (
                    obj.type, self.muast, name))

        for texture in self.map_texture_sprite:
            if self.verbose:
                logging.info("%s: matching %s: %s" % (
                    self.muast, texture, self.map_texture_sprite[texture]))
            # if len(self.map_texture_sprite[texture]) == 1:
            #     sprite = self.map_texture_sprite[texture][0]
            #     self.push_img(texture, self.sprites[sprite])
            # else:
            for sprite in self.map_texture_sprite[texture]:
                self.push_img(sprite, self.sprites[sprite])

        if not flag:
            if self.verbose:
                logging.info(
                    "Unpacking %s meets undefined types. Force getting Texture2Ds." % self.muast)
            object_list = [
                obj
                for path_id, obj in self.objects.items()
                if isinstance(path_id, int)
            ]
            for obj in object_list:
                data = obj.read()
                if obj.type == 'Texture2D':
                    path = os.path.splitext(name)[0] + '/' + data.name
                    img = self._Texture2D(data)
                    self.push_img(path, img)

        self.pop_imgs()

        if 'muast' in self.types:
            muast_path = 'assets/app/staticresources/bundlesource/' + asset_bundle['m_AssetBundleName']
            os.makedirs(os.path.dirname(muast_path), exist_ok=True)
            shutil.copy(self.muast, muast_path)

        return flag

    def push_img(self, path, img):
        if path.endswith("_a"):
            common_name = path[:-2]
            if common_name in self.image_rgb_dict:
                self.save_img(
                    common_name,
                    combine_rgba(self.image_rgb_dict.pop(common_name), img),
                    True)
            else:
                self.image_alpha_dict[common_name] = img

        elif path.endswith("_compa"):
            common_name = path[:-6]
            if common_name in self.image_rgb_dict:
                self.save_img(
                    common_name,
                    combine_rgba(self.image_rgb_dict.pop(common_name), img),
                    True)
            else:
                self.image_alpha_dict[common_name] = img

        elif path.endswith("_rgb"):
            common_name = path[:-4]
            if common_name in self.image_alpha_dict:
                self.save_img(
                    common_name,
                    combine_rgba(img, self.image_alpha_dict.pop(common_name)),
                    True)
            else:
                self.image_rgb_dict[common_name] = img

        else:
            self.save_img(path, img)

    def pop_imgs(self):
        for common_name in self.image_alpha_dict:
            self.save_img(common_name + "_a",
                          self.image_alpha_dict[common_name])
        for common_name in self.image_rgb_dict:
            self.save_img(common_name + "_rgb",
                          self.image_rgb_dict[common_name])

    def save_img(self, path, img, trans_back=False):
        path = os.path.splitext(path)[0]
        if img is None:
            return
        os.makedirs(os.path.dirname(path), exist_ok=True)
        img = ImageOps.flip(img)

        if 'png' in self.types:
            img.save(path + '.png')

        if 'jpg' in self.types:
            if img.mode == 'RGBA':
                background = Image.new("RGBA", img.size, '#FFFFFFFF')
                background.paste(img, (0, 0), img)
                jpg = background.convert("RGB")
            else:
                jpg = img.copy()
            if jpg.size[0] * jpg.size[1] > 0x20000:
                jpg.thumbnail(
                    (jpg.size[0]//2, jpg.size[1]//2), Image.ANTIALIAS)
            jpg.save(path + '.jpg', progressive=True,
                     quality=60, optimize=True)

        if 'icon' in self.types:
            icon = gen_thumbnail(img, trans_back)
            icon.save(path + '.icon.jpg', quality=60, optimize=True)

    def save_json(self, path, data):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(data, f, cls=KiraraJSONEncoder,
                      ensure_ascii=False, default=str)
