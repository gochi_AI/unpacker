from unpack.unpack import Unpacker


def unpack(*args, **kwargs):
    return Unpacker(*args, **kwargs)()
