from PIL import Image
from PIL import ImageStat

# Size of output thumbnail image, pixels
ICON_SIZE = 96

# Parameter of transparent background
STEP = 12
ALPHA1 = 0xC0
ALPHA2 = 0x90

# Thresholds of transparent image detection
VALUE_THR = 210

# Initialize a transparent background pattern
pattern_image = Image.new('RGBA', (ICON_SIZE // STEP, ICON_SIZE // STEP))
data_ptr = pattern_image.load()
for y in range(ICON_SIZE // STEP):
    for x in range(ICON_SIZE // STEP):
        data_ptr[x, y] = (ALPHA2, ALPHA2, ALPHA2, 255) if (
            x + y) % 2 else (ALPHA1, ALPHA1, ALPHA1, 255)
BACK_IMAGE = pattern_image.resize((ICON_SIZE, ICON_SIZE), Image.NEAREST)


def gen_thumbnail(source_image, trans_back=False):
    # \brief Generate a thumbnail icon image
    # \param source_image source image in RGB or RGBA format PIL Image object
    # \param trans_back forced to use a transparent background
    # \return thumbnail image in RGB format, PIL Image object

    thumb_image = source_image.copy()
    thumb_image.thumbnail((ICON_SIZE, ICON_SIZE), Image.ANTIALIAS)

    if (thumb_image.mode == 'RGBA'):
        data_ptr = thumb_image.load()

        value_sum_edge = 0
        pixel_num_edge = 0
        # Manual dilation for alpha channel
        for y in range(1, thumb_image.size[1] - 1):
            for x in range(1, thumb_image.size[0] - 1):
                r, g, b, a = data_ptr[x, y]
                _, _, _, a0 = data_ptr[x, y - 1]
                _, _, _, a1 = data_ptr[x, y + 1]
                _, _, _, a2 = data_ptr[x - 1, y]
                _, _, _, a3 = data_ptr[x + 1, y]

                # Calculate average value of pixels
                # that is on the outer boundary of alpha channel
                if (a and (
                    (a0 > 0) ^ (a1 > 0) or (a2 > 0) ^ (a3 > 0)
                )):
                    pixel_num_edge += 3
                    value_sum_edge += r + g + b

        value_sum_all = 0
        pixel_num_all = 0
        # Extra check of image's non-transparent part

        # Summation on non-transparent part
        for y in range(thumb_image.size[1]):
            for x in range(thumb_image.size[0]):
                r, g, b, a = data_ptr[x, y]
                if (a):
                    pixel_num_all += 3
                    value_sum_all += r + g + b

        # Calculate major characteristic
        if (pixel_num_edge != 0):
            value_mean = value_sum_edge / pixel_num_edge
        # Calculate secondary characteristic
        elif (pixel_num_all != 0):
            value_mean = value_sum_all / pixel_num_all
        # Mark as White
        else:
            value_mean = 0xFF

        if (value_mean > VALUE_THR or trans_back):
            dst_image = BACK_IMAGE.copy()
        else:
            dst_image = Image.new('RGBA', (ICON_SIZE, ICON_SIZE), 0xFFFFFFFF)

        dst_image.paste(thumb_image, (
            (ICON_SIZE - thumb_image.size[0]) // 2,
            (ICON_SIZE - thumb_image.size[1]) // 2
        ), thumb_image)

        return dst_image.convert('RGB')

    else:
        return thumb_image


def combine_rgba(rgb_image, alpha_image):
    # \brief Combine a rgb and an alpha image into a rgba image
    # \param rgb_image source image containing RGB channels, PIL Image object
    # \param alpha_image source image containing A channel, PIL Image object
    # \return Combined image in RGBA format, PIL Image object

    try:
        resized_alpha_image = alpha_image.resize(
            rgb_image.size, Image.BILINEAR)
        r, g, b = rgb_image.split()
        if resized_alpha_image.mode == 'rgb':
            a = resized_alpha_image.convert('L')
        elif resized_alpha_image.mode == 'rgba':
            a = resized_alpha_image.split()[3]
        elif resized_alpha_image.mode == 'L':
            a = resized_alpha_image
        else:
            raise ValueError()
        return Image.merge('RGBA', (r, g, b, a))
    except:
        return rgb_image
