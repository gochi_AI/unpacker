import os
import json
import argparse


args: argparse.Namespace


def list_recursive(path):
    # print('list_recursive', path)
    dirs = set()
    for dir in os.listdir(path):
        if os.path.isdir(path + '/' + dir):
            for name in list_recursive(path + '/' + dir):
                dirs.add(dir + '/' + name)
        elif dir == 'index.json':
            continue
        else:
            if dir.endswith('.icon.jpg'):
                dir = dir[:-9]
            if dir.endswith('.jpg'):
                dir = dir[:-4]
            if dir.endswith('.png'):
                dir = dir[:-4]
            dirs.add(dir)

    return list(dirs)


def handle(path):
    for dir in os.listdir(path):
        dir = path + '/' + dir

        if os.path.isdir(dir):
            if os.path.exists(dir + '.muast'):
                with open(dir + '/' + 'index.json', 'w') as f:
                    json.dump(list_recursive(dir), f)
            else:
                handle(dir)


def main():
    for dir in args.dir:
        handle(dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', nargs='+', default=['public'])
    args = parser.parse_args()
    main()
