set -e

echo 'add ssh key'
which ssh-agent || ( apt-get install -qq openssh-client )
eval $(ssh-agent -s)
mkdir -p ~/.ssh
mv $SSH_PRIVATE_KEY ~/.ssh/id_rsa
chmod 700 ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
git config --global user.email "nilcric@gmail.com"
git config --global user.name "KiraFan Administrator"

echo 'fetch decryptor'
git clone git@gitlab.com:kirafan/assets/seitentranscriptor.git decryptor2

echo 'fetch resource version hash'
git clone git@gitlab.com:kirafan/resource-version-hash.git /go/src/gitlab.com/kirafan/resource-version-hash
cd /go/src/gitlab.com/kirafan/resource-version-hash/krrrvh
go get ./...
go install
cd -
mv /go/bin/krrrvh ./
