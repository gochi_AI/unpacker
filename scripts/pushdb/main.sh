cd $(dirname $0)/../../
dir=$(pwd)

for path in \
    assets/app/staticresources/bundlesource/database \
    assets/app/staticresources/bundlesource/adv/advdatabase \
    assets/app/staticresources/bundlesource/adv/script \
    assets/app/staticresources/bundlesource/prefab/flddb;
do
    cd $dir/$path
    rm -rf *.csv
    python3 $dir/scripts/pushdb/json2csv.py -r $(pwd)
    git add .
    git commit -m "auto update db $(date +%0y%0m%0d)"
    git push
done
