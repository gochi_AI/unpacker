import os
import json
import argparse
from functools import cmp_to_key, reduce


parser = argparse.ArgumentParser()
parser.add_argument("dir", nargs="+", default=["public"])
parser.add_argument("-o", dest="output")
parser.add_argument("-b", dest="bucket", default="texture")
args = parser.parse_args()


def split_number_string(a):
    ans = []
    is_number = False
    current = ''

    def append_result():
        if is_number:
            ans.append(int(current))
        else:
            ans.append(current)

    for char in a:
        current_is_number = char in '0123456789'
        if is_number != current_is_number:
            append_result()
            is_number = current_is_number
            current = ''
        current += char
    else:
        append_result()

    return ans


@cmp_to_key
def compare_file(f0, f1):
    if f0['type'] != 'dir' and f1['type'] == 'dir':
        return 1
    if f0['type'] == 'dir' and f1['type'] != 'dir':
        return -1

    s0 = split_number_string(f0['name'])
    s1 = split_number_string(f1['name'])
    if s0 > s1:
        return 1
    if s0 < s1:
        return -1
    return 0


def handle_dir(name, path, root): return {
    'name': name,
    'type': 'dir',
    'new': True,
}


def handle_png(name, path, root): return {
    'name': name,
    'type': 'png',
    'new': True,
    'link': 'https://{bucket}-asset.kirafan.cn/{uri}.png'.format(
        bucket=args.bucket,
        uri=os.path.join(path, name),
    ),
}


def handle_icon(name, path, root): return {
    'name': name,
    'type': 'png',
    'new': True,
    'icon': 'https://{bucket}-asset.kirafan.cn/{uri}.icon.jpg'.format(
        bucket=args.bucket,
        uri=os.path.join(path, name),
    ),
}


def handle_jpg(name, path, root): return {
    'name': name,
    'type': 'png',
    'new': True,
    'jpg': 'https://{bucket}-asset.kirafan.cn/{uri}.jpg'.format(
        bucket=args.bucket,
        uri=os.path.join(path, name),
    ),
}


def handle_json(name, path, root): return {
    'name': name,
    'type': 'json',
    'new': True,
    'link': 'https://{bucket}-asset.kirafan.cn/{uri}.json'.format(
        bucket=args.bucket,
        uri=os.path.join(path, name),
    ),
}


def handle_csv(name, path, root): return {
    'name': name,
    'type': 'json',
    'new': True,
    'csv': 'https://{bucket}-asset.kirafan.cn/{uri}.csv'.format(
        bucket=args.bucket,
        uri=os.path.join(path, name),
    ),
}


def handle_mp3(name, path, root): return {
    'name': name,
    'type': 'mp3',
    'new': True,
    'link': 'https://{bucket}-asset.kirafan.cn/{uri}.mp3'.format(
        bucket=args.bucket,
        uri=os.path.join(path, name),
    ),
}


def handle_ignore(name, path, root): return None


handle_functions = [
    ('index.json', handle_ignore),
    ('index.csv', handle_ignore),
    ('.png', handle_png),
    ('.icon.jpg', handle_icon),
    ('.jpg', handle_jpg),
    ('.json', handle_json),
    ('.csv', handle_csv),
    ('.mp3', handle_mp3),
]

new = False


def handle(path, root, pre_check=False):
    global new
    if pre_check and new:
        return

    files = {}
    for file in os.listdir(os.path.join(root, path)):
        if os.path.isdir(os.path.join(root, path, file)):
            data = handle_dir(file, path, root)
            if not data:
                continue
            if pre_check:
                handle(os.path.join(path, file), root, True)
            else:
                data['new_items'] = handle(os.path.join(path, file), root)
            if file not in files:
                files[file] = data
            else:
                files[file].update(data)
            continue

        for ext, function in handle_functions:
            if file.endswith(ext):
                name = file[:-len(ext)]
                data = function(name, path, root)
                if not data:
                    break
                if name not in files:
                    files[name] = data
                else:
                    files[name].update(data)
                break

    try:
        with open(os.path.join(args.output, root, path, 'index.json')) as f:
            old_files = json.load(f)
            for file in old_files:
                if file['name'] in files:
                    if pre_check or new:
                        files[file['name']]['new'] = False
                    else:
                        files[file['name']]['new'] = file.get('new', False)

        count = reduce(lambda x, y: x + y, (
            files[file]['new'] +
            ('new_items' in files[file] and files[file]['new_items'] > 0)
            for file in files
        ), 0)

        if pre_check and count:
            new = True
    except:
        count = 0

    if pre_check:
        return

    files = files.values()
    files = filter(lambda x: x, files)
    files = sorted(files, key=compare_file)

    with open(os.path.join(root, path, 'index.json'), 'w') as f:
        json.dump(files, f, ensure_ascii=False)

    if args.output:
        os.makedirs(os.path.join(args.output, root, path), exist_ok=True)
        with open(os.path.join(args.output, root, path, 'index.json'), 'w') as f:
            json.dump(files, f, ensure_ascii=False)

    return count


def main():
    for path in args.dir:
        handle('', path, True)
    print('new =', new)
    for path in args.dir:
        handle('', path)


if __name__ == '__main__':
    main()
