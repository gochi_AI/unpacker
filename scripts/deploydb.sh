set -e

echo 'parsing database'
pip install requests
python unpacker/scripts/parsedb/formal_status.py
python unpacker/scripts/parsedb/cross_adv.py
python unpacker/scripts/parsedb/distributed.py  # dependence: formal_status
python unpacker/scripts/parsedb/period_limited.py  # dependence: distributed
python unpacker/scripts/parsedb/primary_character.py  # dependence: period_limited
python unpacker/scripts/parsedb/item_category.py
python unpacker/scripts/parsedb/weapon.py
python unpacker/scripts/parsedb/full_name.py
python unpacker/scripts/parsedb/quest.py
python unpacker/scripts/parsedb/quest_meta.py  # dependence: quest
python unpacker/scripts/parsedb/item_event_bonus.py
python unpacker/scripts/parsedb/sap.py
python unpacker/scripts/parsedb/tweet_list.py
python unpacker/scripts/parsedb/battle_ai.py
python unpacker/scripts/parsedb/achievement.py
python unpacker/scripts/parsedb/original_characters.py  # dependence: full_name
python unpacker/scripts/parsedb/event_time.py
python unpacker/scripts/parsedb/character_year.py
python unpacker/download/list.py $(unpacker/krrrvh)

echo 'add ssh key'
which ssh-agent || ( apt-get install -qq openssh-client )
eval $(ssh-agent -s)
mkdir -p ~/.ssh
mv $SSH_PRIVATE_KEY ~/.ssh/id_rsa
chmod 700 ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
git config --global user.email "nilcric@gmail.com"
git config --global user.name "KiraFan Administrator"

git clone git@gitlab.com:kirafan/database.git public
rm -rf public/*

mv unpacker/assets/app/staticresources/bundlesource/database public/database
mv unpacker/assets/app/staticresources/bundlesource/adv/advdatabase public/advdatabase
mv unpacker/assets/app/staticresources/bundlesource/prefab/flddb public/flddb
mv assetBundle.json public/
python unpacker/scripts/pushdb/json2csv.py -r public

cd public
set +e
git add .
git commit -m "auto push db$(date +%0y%0m%0d)"
git push
git rev-parse HEAD > version
rm -rf .git
set -e
cd ..

# python unpacker/scripts/index.py public -b ${BUCKET} -o index
