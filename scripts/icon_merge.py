import os
import json
from PIL import Image, ImageOps


pwd = (os.path.dirname(__file__) or '.') + '/../'
database = 'assets/app/staticresources/bundlesource/database/'
charaicon = 'assets/app/staticresources/bundlesource/texture/charauiresource/charaicon/'
mergedcharaicon = 'mergedcharaicon/'
common_ui = 'assets/app/staticresources/bundlesource/uiatlas/commonuiatlas/'

rare_icon_names = [
    'CharaIconFrame03',
    'CharaIconFrame04',
    'CharaIconFrame05',
]
class_icon_names = [
    'ClassIconFighter',
    'ClassIconMagician',
    'ClassIconPriest',
    'ClassIconKnight',
    'ClassIconAlchemist',
]
element_icon_names = [
    'ElementIconFire',
    'ElementIconWater',
    'ElementIconEarth',
    'ElementIconWind',
    'ElementIconMoon',
    'ElementIconSun',
]


def handle():
    rare_icons = [
        Image.open(pwd + common_ui + name + '.png')
        for name in rare_icon_names
    ]
    class_icons = [
        Image.open(pwd + common_ui + name + '.png')
        .resize((32, 32), Image.ANTIALIAS)
        for name in class_icon_names
    ]
    element_icons = [
        Image.open(pwd + common_ui + name + '.png')
        .resize((32, 32), Image.ANTIALIAS)
        for name in element_icon_names
    ]

    with open(pwd + database + 'CharacterList.json') as f:
        character_list = json.load(f)

    os.makedirs(mergedcharaicon, exist_ok=True)

    for character in character_list:
        character_id = character['m_CharaID']

        icon = Image.open(
            pwd + charaicon + 'charaicon_%s/CharaIcon_%s.png' % (character_id, character_id))
        rare_icon = rare_icons[character['m_Rare']-2]
        class_icon = class_icons[character['m_Class']]
        element_icon = element_icons[character['m_Element']]

        merged = Image.new('RGBA', (128, 128), (255, 255, 255))
        merged.paste(icon.crop((3, 3, 125, 125)), (3, 3, 125, 125))
        merged.paste(rare_icon, (0, 0, 128, 128), rare_icon)
        merged.paste(class_icon, (125-32, 125-32, 125, 125), class_icon)
        merged.paste(element_icon, (3, 3, 3+32, 3+32), element_icon)

        merged.save(
            pwd + mergedcharaicon + 'charaicon_%s.png' % character_id)


if __name__ == '__main__':
    handle()
