import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'

formal_statuses = {
    2: (
        (475, 345, 165, 235, 145, 113, 31),
        (375, 160, 310, 140, 220, 93, 31),
        (342, 210, 205, 150, 235, 108, 31),
        (500, 285, 180, 310, 265, 103, 31),
        (400, 175, 240, 190, 195, 98, 31),
    ),
    2.5: (
        (475, 345, 165, 235, 145, 115, 31),
        (375, 160, 310, 140, 220, 95, 31),
        (342, 210, 205, 150, 235, 110, 31),
        (500, 285, 180, 310, 265, 105, 31),
        (400, 175, 240, 190, 195, 100, 31),
    ),
    3: (
        (483, 365, 170, 245, 150, 115, 31),
        (383, 165, 330, 145, 235, 95, 31),
        (350, 220, 215, 160, 245, 110, 31),
        (550, 300, 185, 325, 280, 105, 31),
        (408, 180, 260, 200, 200, 100, 31),
    ),
    4: (
        (500, 390, 175, 255, 155, 117, 31),
        (400, 170, 350, 155, 255, 97, 31),
        (375, 230, 230, 170, 255, 112, 31),
        (633, 320, 190, 345, 295, 107, 31),
        (425, 190, 280, 210, 210, 102, 31),
    )
}
status_keys = (
    'm_InitHp',
    'm_InitAtk',
    'm_InitMgc',
    'm_InitDef',
    'm_InitMDef',
    'm_InitSpd',
    'm_InitLuck'
)


def check_weaken(chara):
    if chara['m_Rare'] != 3:
        return False
    for i, key in enumerate(status_keys[:5]):
        if chara[key] % 10 != formal_statuses[3][chara['m_Class']][i] % 10:
            return True
    return False


def handle():
    with open(pwd + database + 'CharacterList.json') as f:
        character_list = json.load(f)
    with open(pwd + database + 'NamedList.json') as f:
        named_list = json.load(f)

    named_formal_statuses = dict()
    for chara in character_list:
        is_weaken = check_weaken(chara)
        rare = chara['m_Rare'] if not is_weaken else 2.5
        chara['isWeaken'] = is_weaken
        if chara['m_NamedType'] in named_formal_statuses:
            continue
        formal_status = [
            chara[key] - formal_statuses[rare][chara['m_Class']][i]
            for i, key in enumerate(status_keys)
        ]
        named_formal_statuses[chara['m_NamedType']] = formal_status
    for name in named_list:
        try:
            name['formalStatus'] = named_formal_statuses[name['m_NamedType']]
        except KeyError:
            name['formalStatus'] = [0, 0, 0, 0, 0, 0, 0]

    with open(pwd + database + 'CharacterList.json', 'w') as f:
        json.dump(character_list, f, ensure_ascii=False)
    with open(pwd + database + 'NamedList.json', 'w') as f:
        json.dump(named_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
