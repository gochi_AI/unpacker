import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'
debug = False


def open_sap(id):
    try:
        with open(pwd + database + 'sap/SAP_%s.json' % id) as f:
            return json.load(f)
    except FileNotFoundError:
        return None


def open_sag(id):
    try:
        with open(pwd + database + 'sag/SAG_%s.json' % id) as f:
            return json.load(f)
    except FileNotFoundError:
        return None


def frame_of_callback_data(key, SAG):
    if not SAG:
        return 0
    data = \
        SAG['m_evEffectProjectile_Straight'] + \
        SAG['m_evEffectProjectile_Parabola'] + \
        SAG['m_evEffectProjectile_Penetrate'] + \
        SAG['m_evWeaponThrow_Parabola']
    for item in data:
        if (key == item['m_CallbackKey']):
            return item['m_Frame']

    raise KeyError('KeyError when find %s in SAG %s. ' %
                   (key, SAG['m_ID']))


def handle():
    skill_list_names = [
        'SkillList_CARD',
        'SkillList_EN',
        'SkillList_MST',
        'SkillList_PL',
        'SkillList_WPN',
    ]

    for skill_list_name in skill_list_names:
        with open(pwd + database + skill_list_name + '.json') as f:
            skill_list = json.load(f)

        for skill in skill_list:
            m_SAP = open_sap(skill['m_SAP'])
            m_SAG = open_sag(skill['m_SAG'])

            if m_SAP is None:
                skill['sap'] = None
                continue

            order = {}

            for item in m_SAP['m_evSolve']:
                frame = item['m_Frame']
                while (frame in order):
                    frame += 1
                order[frame] = item

            for item in m_SAP['m_CallbackDatas']:
                try:
                    frame = frame_of_callback_data(item['m_Key'], m_SAG)
                except KeyError as e:
                    if debug:
                        print(e)
                    continue
                for i in item['m_evSolve']:
                    while (frame in order):
                        frame += 1
                    order[frame] = i

            skill['sap'] = []

            for frame in sorted(order.keys()):
                item = order[frame]
                if (len(skill['sap']) > 0 and skill['sap'][-1]['index'] == item['m_Index']):
                    skill['sap'][-1]['option'].append(item['m_Option_0'])
                else:
                    skill['sap'].append({
                        'index': item['m_Index'],
                        'option': [item['m_Option_0']]
                    })

        with open(pwd + database + skill_list_name + '.json', 'w') as f:
            json.dump(skill_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
