import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'NamedList.json') as f:
        named_list = json.load(f)

    tweet_list = []

    for named in named_list:
        try:
            with open(pwd + database + 'TweetList_%04d.json' % named['m_NamedType']) as f:
                tweets = json.load(f)
        except OSError:
            continue

        for tweet in tweets:
            tweet['m_NamedType'] = named['m_NamedType']
            tweet_list.append(tweet)

    with open(pwd + database + 'TweetList.json', 'w') as f:
        json.dump(tweet_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
