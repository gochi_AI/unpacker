import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'OriginalCharaLibraryList.json') as f:
        original_character_list = json.load(f)
    with open(pwd + database + 'NamedList.json') as f:
        named_list = json.load(f)

    for named in named_list:
        for character in original_character_list:
            if named['fullName'] != character['m_Title']:
                continue
            character['namedType'] = named['m_NamedType']
            named['originalCharaID'] = character['m_ID']
            break

    with open(pwd + database + 'OriginalCharaLibraryList.json', 'w') as f:
        json.dump(original_character_list, f, ensure_ascii=False)

    with open(pwd + database + 'NamedList.json', 'w') as f:
        json.dump(named_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
