import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'WeaponList.json') as f:
        weapon_list = json.load(f)

    weapon_ids = set(weapon['m_ID'] for weapon in weapon_list)

    for weapon in weapon_list:
        weapon['default'] = (
            weapon['m_ID'] in (1000, 1100, 1200, 1300, 1400) or
            'デフォルトぶき' in weapon['m_WeaponName'] or
            '汎用ぶき' in weapon['m_WeaponName'])

    for weapon in weapon_list:
        if weapon['default']:
            weapon['maxEvolution'] = 0

        elif weapon['m_ID'] < 1000000:
            weapon['maxEvolution'] = 0

        else:
            for id in range(weapon['m_ID'], weapon['m_ID']+10):
                if id in weapon_ids:
                    continue
                else:
                    weapon['maxEvolution'] = id % 10 - 1
                    break

    with open(pwd + database + 'WeaponList.json', 'w') as f:
        json.dump(weapon_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
