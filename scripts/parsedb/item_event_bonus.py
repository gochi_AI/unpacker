import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'ItemList.json') as f:
        item_list = json.load(f)
    with open(pwd + database + 'EventQuestDropExt.json') as f:
        event_quest_drop_ext = json.load(f)

    event_bonus_item_ids = set()
    for item in event_quest_drop_ext:
        for item_id in item['m_ItemIDs']:
            if item_id == -1:
                continue
            event_bonus_item_ids.add(item_id)

    for item in item_list:
        item['isEventBonus'] = item['m_ID'] in event_bonus_item_ids

    with open(pwd + database + 'ItemList.json', 'w') as f:
        json.dump(item_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
