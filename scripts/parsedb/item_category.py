import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'ItemList.json') as f:
        item_list = json.load(f)

    for item in item_list:
        if item['m_Type'] == 0:  # 強化素材
            if item['m_ID'] in range(2000, 4000):
                item['category'] = item['m_Rare'] + 0
            else:
                raise ValueError

        elif item['m_Type'] == 1:  # 進化素材
            if item['m_ID'] in range(6000, 7000):  # 進化珠
                item['category'] = item['m_Rare'] + 100
            elif item['m_ID'] in range(7000, 8000):  # スタチュー
                item['category'] = item['m_Rare'] + 110
            elif item['m_ID'] in range(8000, 9000):  # タイトル別
                item['category'] = item['m_Rare'] + 120
            else:
                raise ValueError

        elif item['m_Type'] == 2:  # 限界突破素材
            if item['m_ID'] in range(4000, 5000):  # 普通
                item['category'] = item['m_Rare'] + 200
            elif item['m_ID'] in range(5000, 6000):
                item['category'] = item['m_Rare'] + 210
            else:
                raise ValueError

        elif item['m_Type'] == 3:  # ぶき素材
            if item['m_ID'] in range(1, 6):  # シンボル
                item['category'] = 300
            elif item['m_ID'] in range(900, 905):  # クレスト
                item['category'] = 301
            elif item['m_ID'] in range(6, 38):  # 素材
                item['category'] = (item['m_ID'] - 2) // 4 + 310
            else:
                raise ValueError

        elif item['m_ID'] in range(100000, 200000):  # イベント素材
            item['category'] = item['m_ID'] // 100

        else:  # そのほか
            item['category'] = item['m_Type'] + 400

        item['type'] = item['category'] // 100

    with open(pwd + database + 'ItemList.json', 'w') as f:
        json.dump(item_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
