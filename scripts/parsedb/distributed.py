import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'

old_distributed_character_ids = [
    18011010, 18011011,
    14021010, 14021011,
    17001010, 17001011,
    11011010, 11011011,
    15061000, 15061001,
    23001000, 23001001,
    21001000, 21001001,
    27001000, 27001001,
    16051000, 16051001,
    14011010, 14011011,
    24001000, 24001001,
    22001000, 22001001,
    18001010, 18001011,
    14001010, 14001011,
    29001000, 29001001,
    11051000, 11051001,
    22051000, 22051001,
    28011000, 28011001,
    20051000, 20051001,
    31001000, 31001001,
    30001000, 30001001,
    24021010, 24021011,
]


def handle():
    with open(pwd + database + 'CharacterList.json') as f:
        character_list = json.load(f)

    for character in character_list:
        character['isDistributed'] = character['m_CharaID'] in old_distributed_character_ids or character['isWeaken']

    with open(pwd + database + 'CharacterList.json', 'w') as f:
        json.dump(character_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
