import os
import sys
import json
import requests


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


keys = (
    'name', 'bgId',
    'stamina',
    'rewardMoney',
    'rewardUserExp',
    'rewardCharacterExp',
    'rewardFriendshipExp',
    'ex2Amount', 'exId2',
)

configs = [
    {
        "name": "main",
        "url": "https://docs.google.com/spreadsheets/d/e/2PACX-1vR0TitlHKxgge9Fw1cIWM42X-xvvo0FMBVwx32IXRV2Zn2HQJlHze7XBi99PM4RcaiFH4wstxKG3cUg/pub?gid=0&single=true&output=tsv",
        "quest": None
    },
    {
        "name": "event",
        "url": "https://docs.google.com/spreadsheets/d/e/2PACX-1vR0TitlHKxgge9Fw1cIWM42X-xvvo0FMBVwx32IXRV2Zn2HQJlHze7XBi99PM4RcaiFH4wstxKG3cUg/pub?gid=1655196772&single=true&output=tsv",
        "quest": "eventQuest"
    },
    {
        "name": "chara",
        "url": "https://docs.google.com/spreadsheets/d/e/2PACX-1vR0TitlHKxgge9Fw1cIWM42X-xvvo0FMBVwx32IXRV2Zn2HQJlHze7XBi99PM4RcaiFH4wstxKG3cUg/pub?gid=1491364206&single=true&output=tsv",
        "quest": None
    },
    {
        "name": "craft",
        "url": "https://docs.google.com/spreadsheets/d/e/2PACX-1vR0TitlHKxgge9Fw1cIWM42X-xvvo0FMBVwx32IXRV2Zn2HQJlHze7XBi99PM4RcaiFH4wstxKG3cUg/pub?gid=1097137141&single=true&output=tsv",
        "quest": None
    }
]


def handle():
    with open(pwd + database + 'QuestList.json') as f:
        quest_list = json.load(f)

    quests = {
        quest['questID']: quest
        for quest in quest_list
    }

    for config in configs:
        r = requests.get(config['url'])

        for line in r.iter_lines():
            if not line:
                continue
            try:
                id, data = line.decode().split('\t')
                id = int(id)
                data = json.loads(data)
                if id not in quests:
                    continue  # WIP
                quest = data.get(config['quest'], data)
                for key in keys:
                    value = quest['quest'].get(key) or quest.get(key)
                    if value:
                        quests[id][key] = value
            except Exception as e:
                print(line)
                raise(e)

    with open(pwd + database + 'QuestList.json', 'w') as f:
        json.dump(list(quests.values()), f)


if __name__ == '__main__':
    try:
        handle()
    except Exception as e:
        print(e)
