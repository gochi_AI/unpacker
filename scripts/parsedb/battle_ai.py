import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'QuestEnemyList.json') as f:
        quest_enemy_list = json.load(f)

    ai_ids = set()
    for enemy in quest_enemy_list:
        ai_ids.add(enemy['m_AIID'])

    battle_ai_data_list = []

    for ai_id in ai_ids:
        with open(pwd + database + 'battleai/BattleAIData_%s.json' % ai_id) as f:
            battle_ai_data_list.append(json.load(f))

    with open(pwd + database + 'BattleAIDataList.json', 'w') as f:
        json.dump(battle_ai_data_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
