import os
import sys
import subprocess
import random

from PIL import Image, ImageOps
from unitypack.engine.texture import TextureFormat


pwd = os.path.dirname(__file__)

if sys.platform == "darwin":
    command = os.path.join(pwd, 'etcpack_macos %s %s >/dev/null 2>&1')
elif sys.platform == "win32":
    command = os.path.join(pwd, 'etcpack.exe %s %s')
else:
    command = os.path.join(pwd, 'etcpack_linux %s %s >/dev/null 2>&1')

etc_formats = {
    TextureFormat.ETC_RGB4:         0,
    TextureFormat.ETC2_RGB:         1,
    TextureFormat.ETC2_RGBA8:       3,
    TextureFormat.ETC2_RGBA1:       4,
    TextureFormat.EAC_R:            5,
    TextureFormat.EAC_RG:           6,
    TextureFormat.EAC_R_SIGNED:     7,
    TextureFormat.EAC_RG_SIGNED:    8
}


def header(width, height, tformat):
    header = b"\x50\x4B\x4D\x20"
    formatD = etc_formats[tformat]
    version = b"20" if tformat != TextureFormat.ETC_RGB4 else b"10"
    formatB = formatD.to_bytes(2, byteorder="big")
    widthB = width.to_bytes(2, byteorder="big")
    heightB = height.to_bytes(2, byteorder="big")
    return header + version + formatB + widthB + heightB + widthB + heightB


def etcpack(data):
    if data.format in etc_formats:
        magic = "%08x" % random.randrange(0, 0x100000000)
        header_data = header(data.width, data.height, data.format)

        with open(magic + '.pkm', 'wb') as f:
            f.write(header_data + data.image_data)

        cmd = command % (magic + '.pkm', magic + '.ppm')
        subprocess.check_output(cmd, shell=True)
        with open(magic + '.ppm', 'rb') as f:
            img = Image.open(f).copy()

        os.remove(magic + '.pkm')
        os.remove(magic + '.ppm')
    else:
        img = data.image

    return img
