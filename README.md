# KiraFan Assets Unpacker

## Requirements

* Python ≥ 3
* Go ≥ 1.12

## Build

### Unitypack

```sh
pip install unitypack
```

For macOS users, please install [decrunch](https://github.com/kirafanautodec/decrunch) before pip install unitypack.

Windows users may need to install [Microsoft Visual C++ Build Tools](https://go.microsoft.com/fwlink/?LinkId=691126) in order to setup decrunch.

### Decryptor

```sh
git clone https://gitlab.com/kirafan/assets/decryptor.git $GOPATH/src/gitlab.com/kirafan/assets/decryptor
cd $GOPATH/src/gitlab.com/kirafan/assets/decryptor/lib
go build -o libkrrdec.so -buildmode c-shared
cd -  # path to Unpacker
ln -s $GOPATH/src/gitlab.com/kirafan/assets/decryptor
```

Note that Decryptor is a private repo so `go get` does not work.

### Resource Version Hash (optional)

```sh
go get -u gitlab.com/kirafan/resource-version-hash/krrrvh
krrrvh
```

You may need to configure your GOPATH in order to run `krrrvh`.

## Usage

```sh
python main.py $(krrrvh) '.*'
```
